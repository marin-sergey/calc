import { Module } from "vuex";
import CalcApi from "../api/calc";

type Request = {
    operator:string;
    num1:number;
    num2:number;
};

const CalcModule: Module<any, any> = {
    state: {
        active_calc: {
            num1:null,
            num2:null,
            operator:'add',
        },
        result: null,
        error: null,
        history_loading: false,
        history: []
    },

    getters: {
        getResult:  state => {
            return state.error ? state.error : state.result;
        },
        getHistory: state => {
            return state.history;
        },
        getActiveCalc: state => {
            return state.active_calc
        }
    },

    /**
     * Use constants in real application!
     * https://vuex.vuejs.org/guide/mutations.html#using-constants-for-mutation-types
     */
    mutations: {
        calculating: (state) => {
            state.result = null;
            state.error = null;
        },
        calculation_done: (state, data:{result:number}) => {
            state.result = data.result;
        },
        calculation_error: (state, error) => {
            console.log('error', error)
            state.result = null;
            state.error = error;
        },
        history_loading_done: (state, data) => {
            data = data.map(function (item: {operator:string, num1:string, num2:string}) {
                item.num1 = parseFloat(item.num1).toString();
                item.num2 = parseFloat(item.num2).toString();

                return item;
            })
            state.history = data;
            state.history_error = null
        },
        history_loading: (state) => {
            state.history = [];
            state.history_error = null
        },
        history_loading_error: (state, error) => {
            state.history = [];
            state.history_error = error
        },
        restore: (state, data) => {
            state.active_calc = {
                operator: data.operator,
                num1: data.num1,
                num2: data.num2,
            }

            state.result = null
            state.error = null
        },
        updateNum1: (state, num1) => {
            state.active_calc.num1 = num1
        },
        updateNum2: (state, num2) => {
            state.active_calc.num2 = num2
        },
        updateOperator: (state, operator) => {
            state.active_calc.operator = operator
        }
    },

    actions: {
        async calculate({ commit }, request: Request) {
            commit("calculating", true);
            try {
                let response = await CalcApi.calculate(request.operator, request.num1, request.num2);
                commit('calculation_done', response.data);
                return response.data.result;
            } catch (error) {
                commit('calculation_error', error);
                return null;
            }
        },
        async loadHistory({ commit }) {
            commit("history_loading", true);
            try {
                let response = await CalcApi.getHistory();
                commit('history_loading_done', response.data);
                return response.data;
            } catch (error) {
                commit('history_loading_error', error);
                return null;
            }
        },
        restore({ commit }, data) {
            commit('restore', data)
        }

    }
};

export default CalcModule;
