<?php

declare(strict_types=1);

namespace App\Request\Domain\Calculator;

use Symfony\Component\Validator\Constraints as Assert;

class CalcRequest
{
    public const ANY_TYPE_NUMBER = "^(?:-)?([\d\s]+|)(?:\.)?(?:\d+)?$"; // 100, -100, 1 500.300, - 100 500, 100.5, 0.5, .555, -.555
    public const OPERATOR_ADD = 'add';
    public const OPERATOR_SUB = 'sub';
    public const OPERATOR_MUL = 'mul';
    public const OPERATOR_DIV = 'div';

    private float $num1;

    /**
     * @Assert\AtLeastOneOf({
     *     @Assert\NotEqualTo(0),
     *     @Assert\Expression("this.getOperator() !== constant('\\App\\Request\\Domain\\Calculator\\CalcRequest::OPERATOR_DIV')")
     *   },
     *   message="Division by zero",
     *   includeInternalMessages=false
     * )
     */
    private float $num2;

    #[Assert\Choice(
        choices: [self::OPERATOR_ADD, self::OPERATOR_SUB, self::OPERATOR_MUL, self::OPERATOR_DIV],
        message: 'Invalid operator'
    )]
    private string $operator;

    public function __construct(string $operator, int|float $num1, int|float $num2)
    {
        $this->operator = $operator;
        $this->num1 = $num1;
        $this->num2 = $num2;
    }

    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @return int|float
     */
    public function getNum1(): int|float
    {
        return $this->num1;
    }

    /**
     * @return int|float
     */
    public function getNum2(): int|float
    {
        return $this->num2;
    }
}
