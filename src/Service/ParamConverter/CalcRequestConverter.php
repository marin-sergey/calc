<?php

declare(strict_types=1);

namespace App\Service\ParamConverter;

use App\Request\Domain\Calculator\CalcRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Converts incoming request into request object of domain.
 */
class CalcRequestConverter implements ParamConverterInterface
{
    public function apply(Request $request, ParamConverter $configuration)
    {
        $calcRequest = new CalcRequest(
            $request->get('operator'),
            (float) str_replace(' ', '', $request->get('num1')),
            (float) str_replace(' ', '', $request->get('num2'))
        );

        $request->attributes->set($configuration->getName(), $calcRequest);
    }

    public function supports(ParamConverter $configuration)
    {
        return CalcRequest::class === $configuration->getClass();
    }
}
