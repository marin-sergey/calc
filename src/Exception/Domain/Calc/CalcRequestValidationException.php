<?php

declare(strict_types=1);

namespace App\Exception\Domain\Calc;

use Symfony\Component\Validator\ConstraintViolationList;

class CalcRequestValidationException extends \InvalidArgumentException implements CalcExceptionInterface
{
    public function __construct(private ConstraintViolationList $errors, $code = 0, \Throwable $previous = null)
    {
        $message = (string) $errors;
        parent::__construct($message, $code, $previous);
    }

    public function getErrors(): ConstraintViolationList
    {
        return $this->errors;
    }
}
