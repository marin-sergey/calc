<?php

namespace App\Tests\Calc;

use App\Request\Domain\Calculator\CalcRequest;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CalcWebTest extends WebTestCase
{
    /**
     * @dataProvider provideMathOperations
     */
    public function testApi(int|float $num1, string $route, string $operator, int|float $num2, int|float|null $expectedResult, int $expectedErrorStatus = null): void
    {
        $client = static::createClient();

        /**
         * here is some workarounds, because demo routes uses different set of parameters.
         */
        $routeParams = [
            'num1' => $num1,
            'num2' => $num2,
        ];

        if ('api_calc_universal' === $route) {
            $routeParams['operator'] = $operator;
        }

        $url = $this->getContainer()->get('router')->generate($route, $routeParams);

        $crawler = $client->request('GET', $url);

        if ($expectedErrorStatus) {
            $this->assertResponseStatusCodeSame($expectedErrorStatus);
        } else {
            $this->assertResponseIsSuccessful();
            $content = $client->getResponse()->getContent();
            $this->assertJson($content);
            $data = json_decode($content, true);
            $this->assertArrayHasKey('result', $data);
            $this->assertEquals($expectedResult, $data['result']);
        }
    }

    public function provideMathOperations(): \Generator
    {
        // [ 1 + 1 = 2]
        yield [1, 'api_calc_add', CalcRequest::OPERATOR_ADD, 1, 2];
        yield [2, 'api_calc_sub', CalcRequest::OPERATOR_SUB, 1, 1];
        yield [2, 'api_calc_universal', CalcRequest::OPERATOR_MUL, 2, 4];
        yield [8, 'api_calc_universal', CalcRequest::OPERATOR_DIV, 2, 4];
        yield [5, 'api_calc_universal', CalcRequest::OPERATOR_DIV, 0, null, Response::HTTP_UNPROCESSABLE_ENTITY];
    }
}
