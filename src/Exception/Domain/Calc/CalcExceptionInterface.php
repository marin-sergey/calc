<?php

declare(strict_types=1);

namespace App\Exception\Domain\Calc;

interface CalcExceptionInterface extends \Throwable
{
}
