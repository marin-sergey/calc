<?php

declare(strict_types=1);

namespace App\Enum;

use Elao\Enum\AutoDiscoveredValuesTrait;
use Elao\Enum\Enum;

final class OperatorEnum extends Enum
{
    use AutoDiscoveredValuesTrait;

    public const ADD = 'add';
    public const SUB = 'sub';
    public const MUL = 'mul';
    public const DIV = 'div';

    public static function values(): array
    {
        return [
            self::ADD,
            self::SUB,
            self::MUL,
            self::DIV,
        ];
    }
}
