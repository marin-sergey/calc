import Vue from "vue";
import Vuex from "vuex";
import CalcModule from "./calc";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        calc: CalcModule
    }
});