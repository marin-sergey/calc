<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Platforms\SqlitePlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210916194811 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        if ($this->platform instanceof SqlitePlatform) {
            $this->addSql('CREATE TABLE calc_history (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                num1 NUMERIC(65, 20) NOT NULL, 
                num2 NUMERIC(65, 20) NOT NULL,
                operator CHECK( operator IN (\'add\', \'sub\', \'mul\', \'div\') ) NOT NULL 
                --(DC2Type:operator)
                )');
        } else {
            $this->addSql('CREATE TABLE calc_history (
                id INT AUTO_INCREMENT NOT NULL, 
                num1 NUMERIC(65, 20) NOT NULL, 
                num2 NUMERIC(65, 20) NOT NULL, 
                operator ENUM(\'add\', \'sub\', \'mul\', \'div\') NOT NULL COMMENT \'(DC2Type:operator)\', 
                PRIMARY KEY(id)) 
                DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE calc_history');
    }
}
