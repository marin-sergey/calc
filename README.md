### How to start?

1. Install docker & docker-compose
2. Create .env file. Copy variables from `.env.docker` & `.env.dist` and set values. Do not forget to update DATABASE_URL with values relevant to `MYSQL_` env variables.
3. run

```shell
docker-compose up
```
Expect message "[OK] Web server listening" with web address of your web-server. 
Usually it listens on 127.0.0.1 on port defined in `PHP_SF_DEV_SERVER_PORT` env variable. 

Use console output to track logs while development.
4. Open console and login into php-container
```shell
docker-compose exec php bash
```
5. Install Composer requirements from console of php-container 
```shell
composer install
```
6. apply migrations from console of php-container  
```shell
bin/console doctrine:migrations:migrate
```

###Frontend

Use `nodejs` container to execute `yarn` & `npm`. 
```shell
docker-compose exec node bash
```

### Running production image

```shell
docker run -v APP_ENV=prod -v APP_SECRET=secret -p 8080:80 registry.gitlab.com/marin-sergey/calc
```

There are additional environment variables that can change behaviour of application:

- DATABASE_URL
- CORS_ALLOW_ORIGIN

By default, image connect to SQLight database and uses hosts `localhost` & `127.0.0.1`.
Check  [config/environment.yaml](config/environment.yaml) for details.