<?php

declare(strict_types=1);

namespace App\Tests\Calc;

use App\Exception\Domain\Calc\CalcRequestValidationException;
use App\Repository\CalcHistoryRepository;
use App\Request\Domain\Calculator\CalcRequest;
use App\Service\Domain\Calc\CalcService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @group unit
 */
class CalcTest extends TestCase
{
    /**
     * @dataProvider provideMathOperations
     */
    public function testRequestValidation(int|float $num1, string $operator, int|float $num2, int|float|null $expectedResult, string $exception = null): void
    {
        $calcRequest = new CalcRequest($operator, $num1, $num2);

        $validator = $this->getValidator();

        $errors = $validator->validate($calcRequest);

        if ($exception) {
            $this->assertEquals($errors->count(), 1);
        } else {
            $this->assertEquals($errors->count(), 0);
        }
    }

    /**
     * @dataProvider provideMathOperations
     */
    public function testCalcService(int|float $num1, string $operator, int|float $num2, int|float|null $expectedResult, string $exception = null): void
    {
        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())->method('persist');
        $em->expects($this->any())->method('flush');
        $calcHistoryRepository = $this->createMock(CalcHistoryRepository::class);
        $parameterBag = $this->createMock(ParameterBag::class);
        $logger = new NullLogger();

        $service = new CalcService($this->getValidator(), $em, $calcHistoryRepository, $parameterBag, $logger);

        $calcRequest = new CalcRequest($operator, $num1, $num2);

        if ($exception) {
            $this->expectException($exception);
        }

        $result = $service->process($calcRequest, false);

        $this->assertEquals($result, $expectedResult);
    }

    public function provideMathOperations(): \Generator
    {
        // [ 1 + 1 = 2]
        yield [1, CalcRequest::OPERATOR_ADD, 1, 2];
        yield [2, CalcRequest::OPERATOR_SUB, 1, 1];
        yield [2, CalcRequest::OPERATOR_MUL, 2, 4];
        yield [8, CalcRequest::OPERATOR_DIV, 2, 4];
        yield [5, CalcRequest::OPERATOR_DIV, 0, null, CalcRequestValidationException::class];
        yield [4, 'unknown_operator', 2, 2, CalcRequestValidationException::class];
    }

    private function getValidator(): ValidatorInterface
    {
        return Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();
    }
}
