import axios from "axios";
import Routing from "./../Components/SfRouting"

export default {
  calculate(operator: string, num1: number, num2: number) {
    return axios.get(`/api/calc/${operator}/${num1}/${num2}`);
  },
  getHistory() {
    const url = Routing.generate('api_calc_histories_get_collection');
    return axios.get(url, {
      headers: {
        accept: 'application/json'
      }
    });
  }
}