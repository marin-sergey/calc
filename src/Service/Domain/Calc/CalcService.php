<?php

declare(strict_types=1);

namespace App\Service\Domain\Calc;

use App\Entity\CalcHistory;
use App\Enum\OperatorEnum;
use App\Exception\Domain\Calc\CalcRequestValidationException;
use App\Exception\Domain\Calc\InvalidOperationException;
use App\Repository\CalcHistoryRepository;
use App\Request\Domain\Calculator\CalcRequest;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CalcService
{
    public const APP_HISTORY_CLEANUP_TRIGGER_SERVICE = 'service';
    public const APP_HISTORY_CLEANUP_TRIGGER_LISTENER = 'listener';
    public const APP_HISTORY_CLEANUP_DEFAULT_LIMIT = '5';

    public function __construct(
        private ValidatorInterface $validator,
        private EntityManagerInterface $entityManager,
        private CalcHistoryRepository $calcHistoryRepository,
        private ParameterBagInterface $parameterBag,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param bool $canCollectToHistory - should this request be logged into database?
     *
     * @return float|int
     */
    public function process(CalcRequest $request, bool $canCollectToHistory = true): float|int
    {
        /**
         * Domain layer protection.
         * It checks for `invalid operator` and `division by zero`.
         */
        $errors = $this->validator->validate($request);

        if ($errors->count() > 0) {
            throw new CalcRequestValidationException($errors);
        }

        /**
         * Glory for php8 and `match` construction. No more ugly `brake`s!
         */
        $result = match ($request->getOperator()) {
            CalcRequest::OPERATOR_ADD => $request->getNum1() + $request->getNum2(),
            CalcRequest::OPERATOR_SUB => $request->getNum1() - $request->getNum2(),
            CalcRequest::OPERATOR_MUL => $request->getNum1() * $request->getNum2(),
            CalcRequest::OPERATOR_DIV => $request->getNum1() / $request->getNum2(),
            default => throw new InvalidOperationException('Provided type of operator not supported'), // This exception will never be thrown, because `operation` validated above
        };

        if ($canCollectToHistory) {
            $this->storeHistory($request);
        }

        return $result;
    }

    private function storeHistory(CalcRequest $request)
    {
        $calcHistory = (new CalcHistory())
            ->setNum1((string) $request->getNum1())
            ->setNum2((string) $request->getNum2())
            ->setOperator(OperatorEnum::get($request->getOperator()));

        $this->entityManager->persist($calcHistory);
        $this->entityManager->flush();

        if (self::APP_HISTORY_CLEANUP_TRIGGER_SERVICE === $this->parameterBag->get('APP_HISTORY_CLEANUP_TRIGGER')) {
            $removedCount = $this->calcHistoryRepository->cleanupHistory($this->parameterBag->get('APP_HISTORY_CLEANUP_LIMIT'));
            $this->logger->info('Cleanup completed', ['count' => $removedCount]);
        }
    }

    /**
     * @return CalcHistory[]
     */
    public function getHistory(): array
    {
        $limit = $this->parameterBag->get('APP_HISTORY_CLEANUP_LIMIT');

        return $this->calcHistoryRepository->findBy([], ['id' => 'DESC'], $limit);
    }
}
