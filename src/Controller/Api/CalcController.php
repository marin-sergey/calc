<?php

namespace App\Controller\Api;

use App\Exception\Domain\Calc\CalcExceptionInterface;
use App\Request\Domain\Calculator\CalcRequest;
use App\Service\Domain\Calc\CalcService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Calculator controller.
 *
 * `add` and `sub` methods added for demonstration of incoming request processing.
 */
#[Route('/api/calc', name: 'api_calc_')]
class CalcController extends AbstractController
{
    /**
     * Direct instantiation of domain request object.
     */
    #[Route(
        path: '/add/{num1}/{num2}',
        name: 'add',
        requirements: [
            'num1' => CalcRequest::ANY_TYPE_NUMBER,
            'num2' => CalcRequest::ANY_TYPE_NUMBER,
        ],
        methods: ['GET']
    )]
    public function add(Request $request, CalcService $calcService): JsonResponse
    {
        // direct instantiating
        $calcRequest = new CalcRequest(
            CalcRequest::OPERATOR_ADD,
            (float) str_replace(' ', '', $request->get('num1')),
            (float) str_replace(' ', '', $request->get('num2'))
        );

        try {
            $result = $calcService->process($calcRequest, true);
        } catch (CalcExceptionInterface $e) {
            // catching domain exceptions. Expected two types related to validation process.
            throw new UnprocessableEntityHttpException($e->getMessage());
        }

        return $this->json(['result' => $result]);
    }

    /**
     * Domain object instantiation using custom ParamConverter.
     */
    #[Route(
        path: '/sub/{num1}/{num2}',
        name: 'sub',
        requirements: [
            'num1' => CalcRequest::ANY_TYPE_NUMBER,
            'num2' => CalcRequest::ANY_TYPE_NUMBER,
        ],
        defaults: [
            'operator' => CalcRequest::OPERATOR_SUB,
        ],
        methods: ['GET']
    )]
    #[ParamConverter('calcRequest', converter: 'calc_request_converter')]
    public function sub(CalcService $calcService, CalcRequest $calcRequest): JsonResponse
    {
        try {
            $result = $calcService->process($calcRequest, true);
        } catch (CalcExceptionInterface $e) {
            // catching domain exceptions. Expected two types related to validation process.
            throw new UnprocessableEntityHttpException($e->getMessage());
        }

        return $this->json(['result' => $result]);
    }

    /**
     * This action can replace all the methods above.
     */
    #[Route(
        path: '/{operator}/{num1}/{num2}',
        name: 'universal',
        requirements: [
            'num1' => CalcRequest::ANY_TYPE_NUMBER,
            'num2' => CalcRequest::ANY_TYPE_NUMBER,
            'operator' => '^'.CalcRequest::OPERATOR_MUL.'|'.CalcRequest::OPERATOR_DIV.'$',
        ],
        methods: ['GET']
    )]
    #[ParamConverter('calcRequest', converter: 'calc_request_converter')]
    public function muldiv(CalcService $calcService, CalcRequest $calcRequest): JsonResponse
    {
        try {
            $result = $calcService->process($calcRequest, true);
        } catch (CalcExceptionInterface $e) {
            // catching domain exceptions. Expected two types related to validation process.
            throw new UnprocessableEntityHttpException($e->getMessage());
        }

        return $this->json(['result' => $result]);
    }

    /*
     * This controller replaced with API Platform solution
     *
    #[Route(
        path: '/history',
        name: 'history',
        methods: ['GET']
    )]
    public function history(CalcService $calcService): JsonResponse
    {
        $history = $calcService->getHistory();

        return $this->json(['result' => $history]);
    }*/
}
