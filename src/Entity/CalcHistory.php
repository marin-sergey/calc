<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Enum\OperatorEnum;
use App\Repository\CalcHistoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CalcHistoryRepository::class)
 */
#[ApiResource(
    collectionOperations: [
        'get' => [
            'path' => 'calc/history',
            'pagination_maximum_items_per_page' => 5,
            'order' => ['id' => 'DESC'],
        ],
    ],
    itemOperations: ['get' => [
        'path' => 'calc/history/{id}',
    ]],
    shortName: 'calc_history'
)]
class CalcHistory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=20)
     */
    public ?string $num1;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=20)
     */
    private ?string $num2;

    /**
     * @ORM\Column(type="operator")
     */
    private OperatorEnum $operator;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNum1(): ?string
    {
        return $this->num1;
    }

    public function setNum1(string $num1): self
    {
        $this->num1 = $num1;

        return $this;
    }

    public function getNum2(): ?string
    {
        return $this->num2;
    }

    public function setNum2(string $num2): self
    {
        $this->num2 = $num2;

        return $this;
    }

    public function getOperator(): ?OperatorEnum
    {
        return $this->operator;
    }

    public function setOperator(OperatorEnum $operator): self
    {
        $this->operator = $operator;

        return $this;
    }
}
