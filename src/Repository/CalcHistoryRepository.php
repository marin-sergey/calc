<?php

namespace App\Repository;

use App\Entity\CalcHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CalcHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method CalcHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method CalcHistory[]    findAll()
 * @method CalcHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CalcHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CalcHistory::class);
    }

    public function cleanupHistory(int $limit): int
    {
        /**
         * sub-selects with limit+offset not allowed in DQL.
         *
         * https://github.com/doctrine/orm/issues/5409
         *
         * So, it can do it with pure sql or two queries
         */
        $removedCount = 0;

        $q = $this->createQueryBuilder('h')
            ->select('h.id')
            ->orderBy('h.id', 'DESC')
            ->setFirstResult($limit)
            ->setMaxResults(1);

        if ($id = $q->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR)) {
            $removedCount = $this->createQueryBuilder('h')
                ->delete()
                ->where('h.id <= :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->execute();
        }

        return $removedCount;
    }
}
