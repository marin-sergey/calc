<?php

declare(strict_types=1);

namespace App\Exception\Domain\Calc;

class InvalidOperationException extends \InvalidArgumentException implements CalcExceptionInterface
{
}
