import Vue from "vue";

import VueRouter from "vue-router";
import Home from "./views/Home.vue";
import History from "./views/History.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [
    { path: "/", component: Home },
    { path: "/history", component: History },
    { path: "*", redirect: "/" }
  ]
});